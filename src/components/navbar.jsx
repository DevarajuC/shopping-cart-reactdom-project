import React from "react";

//Stateless functional Component
const NavBar = ({ totalCounters }) => {
  console.log("NavBar rendered");
  return (
    <nav className="navbar navbar-light bg-light">
      <p className="navbar-brand">
        Navbar
        <span className="badge badge-pill badge-secondary ml-2">
          {totalCounters}
        </span>
      </p>
    </nav>
  );
};

export default NavBar;
